Command
=======

command.id - Id команды.

command.name - Название команды.

command.type - Тип команды.

command.triggers - Триггеры команды.

command.messages - Сообщения команды.

command.userlevel - Уровень доступа команды.

command.count - Количество использований команды.

command.cooldown - Откат команды.

command.last_call_date - Время последнего вызова команды в unix формате.

