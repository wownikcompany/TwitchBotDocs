Message
=======

message.time - Время отправки сообщения.

message.chan - Текущий канал.

message.user - Отправитель.

message.text - Текст сообщения.

message.converted_text - Конвертированный в другую раскладку текст сообщения.

message.badges - twitch-badges.

message.bits - twitch-bits.

message.color - Цвет ника отправителя.

message.display_name - Отображаемое имя отправителя.

message.emotes - .

message.id - twitch_id сообщения

message.mod - Модер ли отправитель?

message.room_id - room_id

message.subscriber - Подписчик ли отправитель?

message.tmi_sent_ts - tmi_sent_ts

message.turbo - turbo

message.user_id - twitch-user_id

message.user_type - user_type
